import {ADD_DECODE, ADD_ENCODE, ON_CHANGE} from "./actions";

const initialState = {
    password: '',
    decode: '',
    encode: ''
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_ENCODE:
            return {...state, encode: action.data};
        case ADD_DECODE:
            return {...state, decode: action.data};
        case ON_CHANGE:
            return {...state, [action.e.name]: action.e.value};
        default:
            return state;

    }
};

export default reducer;