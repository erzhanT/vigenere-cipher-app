import axiosApi from "../axiosApi";

export const ADD_ENCODE = 'ADD_ENCODE';
export const ADD_DECODE = 'ADD_DECODE';
export const ON_CHANGE = 'ON_CHANGE';


export const addEncode = data => ({type: ADD_ENCODE, data});
export const addDecode = data => ({type: ADD_DECODE, data});
export const onChange = e => ({type: ON_CHANGE, e});

export const addCipher = data => {
    return async dispatch => {
        const object = {
            password: data.password,
            message: data.encode
        }
        const response = await axiosApi.post('/encode', object);
        dispatch(addDecode(response.data));
    };
};

export const addDecipher = data => {
    return async dispatch => {
        const object = {
            password: data.password,
            message: data.decode
        }
        const response = await axiosApi.post('/decode', object);
        dispatch(addEncode(response.data));
    };
};