import React from "react";
import './App.css';
import Container from "@material-ui/core/Container";
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import TextField from '@material-ui/core/TextField';
import {createStyles, makeStyles} from '@material-ui/core/styles';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import {useDispatch, useSelector} from "react-redux";
import {addCipher, addDecipher, onChange} from "./store/actions";


const useStyles = makeStyles(theme =>
    createStyles({
        root: {
            margin: theme.spacing(1),
            width: '25ch',
        },
        gridMargin: {
            margin: theme.spacing(5),
        }
    }),
);

const App = () => {
    const classes = useStyles();

    const dispatch = useDispatch();
    const state = useSelector(state => state);


    const onChangeHandler = (e) => {
        dispatch(onChange(e.target));
    };

    const postCipher = () => {
        dispatch(addCipher(state));
    }

    const postDecipher = () => {
        dispatch(addDecipher(state));
    }

    return (
        <>
            <CssBaseline/>
            <main>
                <Container>
                    <Grid container direction={"column"} spacing={2} className={classes.gridMargin}>
                        <Grid item>
                            <TextField
                                multiline
                                rows={5}
                                id="filled-basic"
                                label="Encode message"
                                variant="outlined"
                                name='encode'
                                value={state.encode}
                                onChange={onChangeHandler}
                            />
                        </Grid>
                        <Grid item>
                            <TextField
                                id="filled-basic"
                                label="Password"
                                variant="outlined"
                                name='password'
                                value={state.password}
                                onChange={onChangeHandler}
                            />
                            <IconButton
                                className={classes.margin}
                                size="medium"
                                onClick={postDecipher}
                            >
                                <ArrowUpwardIcon fontSize="inherit"/>
                            </IconButton>
                            <IconButton
                                className={classes.margin}
                                size="medium"
                                onClick={postCipher}
                            >
                                <ArrowDownwardIcon fontSize="inherit"/>
                            </IconButton>
                        </Grid>
                        <Grid item>
                            <TextField
                                multiline
                                rows={5}
                                id="filled-basic"
                                label="Decode message"
                                variant="outlined"
                                name='decode'
                                value={state.decode}
                                onChange={onChangeHandler}
                            />
                        </Grid>
                    </Grid>
                </Container>
            </main>

        </>
    )

};

export default App;
