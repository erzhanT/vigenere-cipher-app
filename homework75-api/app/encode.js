const express = require("express");
const Vigenere = require('caesar-salad').Vigenere;
const router = express.Router();



router.post('/', (req, res) => {
    res.send(Vigenere.Cipher(req.body.password).crypt(req.body.message));
});


module.exports = router;
