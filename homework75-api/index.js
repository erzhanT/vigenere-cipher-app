const express = require("express");
const cors = require("cors");
const port = 8000;
const encode = require('./app/encode');
const decode = require('./app/decode');

const app = express();
app.use(express.json());
app.use(cors());


app.use('/encode', encode);
app.use('/decode', decode);


app.listen(port, () => {
    console.log('We are live on ' + port);
});

